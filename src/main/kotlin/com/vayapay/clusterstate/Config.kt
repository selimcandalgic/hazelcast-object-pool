package com.vayapay.clusterstate

import com.hazelcast.config.Config
import com.hazelcast.config.ListenerConfig
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class CacheConfig {

    @Bean
    fun hazelcastConfig(ctx: ApplicationContext): Config {
        val config = Config()
        config.instanceName = "testInstance"
        config.clusterName = "testCluster"
        config.listenerConfigs.add(ListenerConfig(ClusterMembershipListener(ctx)))
        config.listenerConfigs.add(ListenerConfig(ClusterLifecycleListener(ctx)))
        return config
    }

}