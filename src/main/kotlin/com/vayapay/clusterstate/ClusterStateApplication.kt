package com.vayapay.clusterstate

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ClusterStateApplication : CommandLineRunner {
    @Autowired
    lateinit var objectPool: CachedObjectPool<Int, String>
    lateinit var borrowed: Pair<Int, String>
    override fun run(vararg args: String?) {
        objectPool.initCache(*arrayOf(Pair(1, "1"), Pair(2, "2"), Pair(3, "3")))
        borrowed = objectPool.borrowObject()
    }
}

fun main(args: Array<String>) {
    runApplication<ClusterStateApplication>(*args)
}
