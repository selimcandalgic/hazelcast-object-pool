package com.vayapay.clusterstate

import com.hazelcast.cluster.MembershipEvent
import com.hazelcast.cluster.MembershipListener
import com.hazelcast.core.HazelcastInstance
import com.hazelcast.core.LifecycleEvent
import com.hazelcast.core.LifecycleListener
import mu.KotlinLogging
import org.springframework.context.ApplicationContext
import org.springframework.context.event.ContextStoppedEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Service
import java.io.Serializable

const val AVAILABLE = "available"
const val BORROWED = "borrowed"

data class CacheWrapper<V : Serializable>(
    val value: V,
    val memberId: String? = null
) : Serializable

class ClusterLifecycleListener(
    private val ctx: ApplicationContext
) : LifecycleListener {
    override fun stateChanged(event: LifecycleEvent) {
        if (event.state == LifecycleEvent.LifecycleState.SHUTTING_DOWN) {
            val cache = ctx.getBean("cachedObjectPool") as CachedObjectPool<Serializable, Serializable>
            cache.releaseObjects()
        }
    }

}

class ClusterMembershipListener(
    private val ctx: ApplicationContext
) : MembershipListener {

    override fun memberAdded(event: MembershipEvent) {

    }

    override fun memberRemoved(event: MembershipEvent) {
        val cache = ctx.getBean("cachedObjectPool") as CachedObjectPool<Serializable, Serializable>
        cache.releaseObjects(event.member.uuid.toString())
    }
}

@Service
class CachedObjectPool<K : Serializable, V : Serializable>(
    private val hazelcastInstance: HazelcastInstance
) {
    private val semaphore = hazelcastInstance.cpSubsystem.getSemaphore("pool")
    private val logger = KotlinLogging.logger {}

    init {
        semaphore.init(1)
    }

    @EventListener
    fun releaseObject(event: ContextStoppedEvent) {
        releaseObjects(hazelcastInstance.cluster.localMember.uuid.toString())
    }

    fun initCache(vararg values: Pair<K, V>) {
        val tx = hazelcastInstance.newTransactionContext()
        semaphore.acquire()
        try {
            logger.info { "initializing cache" }
            tx.beginTransaction()
            val availableCache = tx.getMap<K, V>(AVAILABLE)
            val borrowedCache = tx.getMap<K, CacheWrapper<V>>(BORROWED)
            values.forEach {
                if (!availableCache.containsKey(it.first) &&
                    !borrowedCache.containsKey(it.first)
                ) {
                    availableCache.put(it.first, it.second)
                    logger.info { "added ${it.first} to cache" }
                } else {
                    logger.info { "cache already initialized ${it.first}, skipping" }
                }
            }
            tx.commitTransaction()
            logger.info { "done initializing cache" }
        } catch (t: Throwable) {
            logger.warn(t) { "exception when initializing cache" }
            tx.rollbackTransaction()
            throw t
        } finally {
            semaphore.release()
        }
    }

    fun borrowObject(): Pair<K, V> {
        val tx = hazelcastInstance.newTransactionContext()
        val memberId = hazelcastInstance.cluster.localMember.uuid.toString()
        semaphore.acquire()
        try {
            logger.info { "borrowing object" }
            tx.beginTransaction()
            val availableCache = tx.getMap<K, V>(AVAILABLE)
            val borrowedCache = tx.getMap<K, CacheWrapper<V>>(BORROWED)
            val key = availableCache.keySet().first()
            val value = availableCache.remove(key)
            borrowedCache.put(key, CacheWrapper(value, memberId))
            tx.commitTransaction()
            val borrowed = Pair(key, value)
            logger.info { "done borrowing object $borrowed" }
            return borrowed
        } catch (t: Throwable) {
            logger.warn(t) { "exception when borrowing object" }
            tx.rollbackTransaction()
            throw t
        } finally {
            semaphore.release()
        }
    }

    fun releaseObjects() {
        val memberId = hazelcastInstance.cluster.localMember.uuid.toString()
        releaseObjects(memberId)
    }

    fun releaseObjects(memberId: String) {
        val tx = hazelcastInstance.newTransactionContext()
        semaphore.acquire()
        try {
            logger.info { "releasing objects for $memberId" }
            tx.beginTransaction()
            val availableCache = tx.getMap<K, V>(AVAILABLE)
            val borrowedCache = tx.getMap<K, CacheWrapper<V>>(BORROWED)
            borrowedCache.keySet().forEach {
                if (borrowedCache[it].memberId == memberId) {
                    val value = borrowedCache.remove(it).value
                    availableCache.put(it, value)
                    logger.info { "released $it" }
                }
            }
            tx.commitTransaction()
            logger.info { "done releasing objects for $memberId" }
        } catch (t: Throwable) {
            logger.warn(t) { "exception when releasing objects for $memberId" }
            tx.rollbackTransaction()
            throw t
        } finally {
            semaphore.release()
        }
    }

    fun releaseObject(key: K) {
        semaphore.acquire()
        val tx = hazelcastInstance.newTransactionContext()
        try {
            logger.info { "releasing object $key" }
            tx.beginTransaction()
            val availableCache = tx.getMap<K, V>(AVAILABLE)
            val borrowedCache = tx.getMap<K, CacheWrapper<V>>(BORROWED)
            val borrowed = borrowedCache.remove(key)
            availableCache.put(key, borrowed.value)
            tx.commitTransaction()
            logger.info { "done releasing object $key" }
        } catch (t: Throwable) {
            logger.warn(t) { "exception when releasing object $key" }
            tx.rollbackTransaction()
            throw t
        } finally {
            semaphore.release()
        }
    }
}